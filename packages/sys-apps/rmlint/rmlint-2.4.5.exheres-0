# Copyright 2015-2016 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require scons
require python [ with_opt=true option_name='X' blacklist='3' ]
require github [ user='sahib' tag="v${PV//_/-}" ]

SUMMARY="Extremely fast tool to remove duplicates and other lint from your filesystem"
SLOT="0"
LICENCES="GPL-3"

PLATFORMS="~amd64"

RESTRICT="test"

MYOPTIONS="
    ( providers: elfutils libelf ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-python/Sphinx
        sys-devel/gettext
    build+run:
        dev-libs/glib[>=2.32]
    run:
        core/json-glib
        sys-apps/util-linux
        providers:elfutils? ( dev-util/elfutils )
        providers:libelf? ( dev-libs/libelf )
"

src_prepare() {
    edo mkdir shim
    for t in ar cc ld ranlib;do
        edo ln -s $(type -fPp $(exhost --tool-prefix)"${t}") shim/"$t"
    done
    export PATH="${WORK}/shim:${PATH}"
    default
}

src_install() {
    escons install DESTDIR="${IMAGE}"   \
        --actual-prefix=/usr            \
        --prefix="${IMAGE}"/usr         \
        --libdir=lib                    \
        --without-gui # if someone wants the gui that badly, they can deal with the awful build system...

    edo gunzip "${IMAGE}"/usr/share/man/man1/rmlint.1.gz

    edo mkdir "${IMAGE}"/usr/$(exhost --target)
    edo mv "${IMAGE}"/usr/{,$(exhost --target)/}bin
    emagicdocs
}


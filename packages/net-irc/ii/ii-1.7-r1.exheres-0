# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="minimalist FIFO and filesystem-based IRC client"
SLOT="0"
LICENCES="MIT"

PLATFORMS="~amd64"

# ssl and ipv6 patches conflict :(
MYOPTIONS="
    action
    (
        ipv6
        ssl
    )   [[ number-selected = at-most-one ]]
    ssl? ( providers: libressl openssl [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build+run:
        ssl? (
            providers:libressl? ( dev-libs/libressl:= )
            providers:openssl? ( dev-libs/openssl )
        )
"

DOWNLOADS="
    http://dl.suckless.org/tools/${PNV}.tar.gz
    action? ( http://tools.suckless.org/${PN}/patches/${PN}-1.4-ctcp_action.diff -> ${PNV}-action.patch )
    ipv6? ( http://tools.suckless.org/${PN}/patches/${PN}-ipv6.diff -> ${PNV}-ipv6.patch )
    ssl? ( http://tools.suckless.org/${PN}/patches/${PNV}-ssl.diff -> ${PNV}-ssl.patch )
"

src_prepare() {
    for opt in ${OPTIONS};do
        case "${opt}" in
            action|ipv6|ssl)
                expatch "${FETCHEDDIR}/${PNV}-${opt}.patch"
            ;;
        esac
    done

    edo sed -e "s#^PREFIX.*#PREFIX = /usr/$(exhost --target)#"  \
            -e "s#^MANDIR.*${PREFIX}#MANDIR = /usr/share/man#"  \
            -e "s#^CC.*##"  \
            -e "s#^DOCDIR.*#DOCDIR = /usr/share/doc/${PNVR}/#"  \
            -e "s#-L/usr/lib#-L/usr/$(exhost --target)/lib#g"   \
            -e "s#-I/usr/include#-I/usr/$(exhost --target)/include#g"   \
            -i config.mk
}

